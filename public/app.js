var input = document.querySelector('.input_text');
var main = document.querySelector('#name');
var temp = document.querySelector('.temp');
var desc = document.querySelector('.desc');
var clouds = document.querySelector('.clouds');
var button= document.querySelector('.submit');

var appId = "d2929e9483efc82c82c32ee7e02d563e";

button.addEventListener('click', function(name){
fetch('https://api.openweathermap.org/data/2.5/weather?q='+input.value+'&appid='+appId)
.then(response => response.json())
.then(data => {
  var tempValue = data['main']['temp'];
  var nameValue = data['name'];
  var descValue = data['weather'][0]['description'];

  main.innerHTML = nameValue;
  desc.innerHTML = "Desc - "+descValue;
  tempValue = tempValue - 273.15 ;
  temp.innerHTML = "Temp - "+Math.round(tempValue)+ " Degree Celsius";
  input.value ="";

})

.catch(err => alert("Please check the spelling of your city name!"));
})
